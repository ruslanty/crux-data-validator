# Crux Data Validator

Simple PHP library for validating input data

#### Requirements:
+ PHP 7.0 or higher
+ Composer (for installation)

#### Features:
+ Validate custom data, e.g., POST
+ Extendable, custom rules
+ Functional tests

#### Installation:
```shell script
composer require ruslanty/crux-data-validator
```
