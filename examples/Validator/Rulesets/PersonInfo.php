<?php

declare(strict_types=1);

namespace CruxDataValidator\Examples\Validator\Rulesets;

final class PersonInfo implements \CruxDataValidator\RuleSetInterface
{
    public function getRuleSet(): array
    {
        return [
            'name' => 'cyrillic_alpha|mb_min_length,1|mb_max_length,100',
            'surname' => 'cyrillic_alpha|mb_min_length,1|mb_max_length,100',
            'email' => 'required_if,surname:Сваггинс|email',
            'phone' => ['regex,/^\d{8}$/'],
        ];
    }
}
