<?php

declare(strict_types=1);

namespace CruxDataValidator\Examples\Validator\Rulesets;

final class CustomData implements \CruxDataValidator\RuleSetInterface
{
    public function getRuleSet(): array
    {
        return [
            'custom_field' => 'custom_rule_name',
        ];
    }
}
