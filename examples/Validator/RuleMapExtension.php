<?php

declare(strict_types=1);

namespace CruxDataValidator\Examples\Validator;

final class RuleMapExtension
{
    public static function getRules(): array
    {
        return [
            'custom_rule_name' => \CruxDataValidator\Examples\Validator\Rules\CustomRuleName::class,
        ];
    }
}
