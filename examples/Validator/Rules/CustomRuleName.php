<?php

declare(strict_types=1);

namespace CruxDataValidator\Examples\Validator\Rules;

final class CustomRuleName implements \CruxDataValidator\Rules\RuleInterface
{
    private $errorMessage = '"{FIELD}" must be a valid format.';

    public function getErrorMessage(): string
    {
        return $this->errorMessage;
    }

    public function getRuleClass(): string
    {
        return __CLASS__;
    }

    public function isValid(string $field, array $parameters, string $argument = null): bool
    {
        if (!array_key_exists($field, $parameters)) {
            return true;
        }

        return (bool)preg_match('~^[a-z0-9]{10}$~', $parameters[$field]);
    }
}
