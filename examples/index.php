<?php

declare(strict_types=1);

ini_set('display_errors', '1');
ini_set('display_startup_errors', '1');
error_reporting(E_ALL);
date_default_timezone_set('UTC');

require __DIR__ . '/../vendor/autoload.php';

echo '<pre>';

// Required fields for validator with custom rule list
const REQUIRED_FIELDS = [
    'name',
    'surname',
    'phone',
];

// Parameters (ex.: $_GET, $_POST)
$parameters = [
    'name' => 'Йоло',
    'surname' => 'Сваггинс',
    'email' => 'vasiliy@pupkin.lv',
    'phone' => '12345678',
    'custom_field' => '1234567890',
];

// Rule list for parameters
$rules = [
    'name' => 'required|cyrillic_alpha|mb_min_length,1|mb_max_length,100',
    'surname' => 'required|cyrillic_alpha|mb_min_length,1|mb_max_length,100',
    'email' => 'required_if,surname:Сваггинс|email',
    'phone' => ['required', 'regex,/^\d{8}$/'], // array can be also used
];

// Custom list with rules
$personInfo = new \CruxDataValidator\Examples\Validator\Rulesets\PersonInfo();
$customData = new \CruxDataValidator\Examples\Validator\Rulesets\CustomData();

// init validator
$validator = new \CruxDataValidator\Validator();

// validate
$valid = $validator->validate($parameters, $rules);
echo is_array($valid) ? customLog('validate', $valid) : 'validate: ok' . PHP_EOL;

// assert
try {
    $validator->assert($parameters, $rules);
    echo 'assert: ok' . PHP_EOL;
} catch (\CruxDataValidator\Exceptions\ValidatorException $e) {
    echo customLog('assert: ' . $e->getMessage(), $e->getErrors());
}

// customValidate
$valid = $validator->customValidate($parameters, [$personInfo], REQUIRED_FIELDS);
echo is_array($valid) ? customLog('validateByRuleset', $valid) : 'validateByRuleset: ok' . PHP_EOL;

// customAssert
try {
    $validator->customAssert($parameters, [$personInfo], REQUIRED_FIELDS);
    echo 'assertByRuleset: ok' . PHP_EOL;
} catch (\CruxDataValidator\Exceptions\ValidatorException $e) {
    echo customLog('assertByRuleset: ' . $e->getMessage(), $e->getErrors());
}

// Extended validator with new rules
$extendedValidator = new \CruxDataValidator\ExtendableValidator(\CruxDataValidator\Examples\Validator\RuleMapExtension::getRules());
$rules['custom_field'] = 'required|custom_rule_name'; // new rule

// extended validate
$valid = $extendedValidator->validate($parameters, $rules);
echo is_array($valid) ? customLog('extendedValidate', $valid) : 'extendedValidate: ok' . PHP_EOL;

// extended assert
try {
    $extendedValidator->assert($parameters, $rules);
    echo 'extendedAssert: ok' . PHP_EOL;
} catch (\CruxDataValidator\Exceptions\ValidatorException $e) {
    echo customLog('extendedAssert: ' . $e->getMessage(), $e->getErrors());
}

// extended customValidate
$valid = $extendedValidator->customValidate($parameters, [$personInfo, $customData], REQUIRED_FIELDS);
echo is_array($valid) ? customLog('extendedValidateByRuleset', $valid) : 'extendedValidateByRuleset: ok' . PHP_EOL;

// extended customAssert
try {
    $extendedValidator->customAssert($parameters, [$personInfo, $customData], REQUIRED_FIELDS);
    echo 'extendedAssertByRuleset: ok' . PHP_EOL;
} catch (\CruxDataValidator\Exceptions\ValidatorException $e) {
    echo customLog('extendedAssertByRuleset: ' . $e->getMessage(), $e->getErrors());
}

function customLog(string $message, array $errors): string
{
    return sprintf(
        '<b>%s</b>' . PHP_EOL . '%s' . PHP_EOL . '%s' . PHP_EOL,
        $message,
        print_r($errors, true),
        str_repeat('-', 10)
    );
}
