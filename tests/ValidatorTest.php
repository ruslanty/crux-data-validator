<?php

declare(strict_types=1);

namespace CruxDataValidator\Tests;

use CruxDataValidator\Validator;
use PHPUnit\Framework\TestCase;

class ValidatorTest extends TestCase
{
    public function testStaticCreateShouldReturnNewValidator()
    {
        $this->assertInstanceOf(Validator::class, new Validator);
    }
}
