<?php

declare(strict_types=1);

namespace CruxDataValidator\Tests\Rules;

use CruxDataValidator\Validator;
use PHPUnit\Framework\TestCase;

final class RequiredTest extends TestCase
{
    public function testValid()
    {
        $parameters = [
            'value1' => 'a',
            'value2' => true,
            'value3' => 1,
        ];

        $rules = [
            'value1' => 'required',
            'value2' => 'required',
            'value3' => 'required',
        ];

        $result = (new Validator())->validate($parameters, $rules);

        $this->assertTrue($result);
    }

    public function testInvalid()
    {
        $parameters = [
            'value1' => '',
            'value2' => null,
        ];

        $rules = [
            'value1' => 'required',
            'value2' => 'required',
        ];

        $result = (new Validator())->validate($parameters, $rules);

        $this->assertNotTrue($result);
        $this->assertArrayHasKey('value1', $result);
        $this->assertArrayHasKey('value2', $result);
    }
}
