<?php

declare(strict_types=1);

namespace CruxDataValidator\Tests\Rules;

use CruxDataValidator\Validator;
use PHPUnit\Framework\TestCase;

final class IntegerTest extends TestCase
{
    public function testValid()
    {
        $parameters = [
            'value1' => '123',
            'value2' => 123,
            'value3' => 0,
        ];

        $rules = [
            'value1' => 'integer',
            'value2' => 'integer',
            'value3' => 'integer',
        ];

        $result = (new Validator())->validate($parameters, $rules);

        $this->assertTrue($result);
    }

    public function testInvalid()
    {
        $parameters = [
            'value1' => '-1',
            'value2' => '0.5',
            'value3' => null,
        ];

        $rules = [
            'value1' => 'integer',
            'value2' => 'integer',
            'value3' => 'integer',
        ];

        $result = (new Validator())->validate($parameters, $rules);

        $this->assertNotTrue($result);
        $this->assertArrayHasKey('value1', $result);
        $this->assertArrayHasKey('value2', $result);
        $this->assertArrayHasKey('value3', $result);
    }
}
