<?php

declare(strict_types=1);

namespace CruxDataValidator\Tests\Rules;

use CruxDataValidator\Validator;
use PHPUnit\Framework\TestCase;

final class ContainsTest extends TestCase
{
    public function testValid()
    {
        $parameters = [
            'value1' => 'bbb',
            'value2' => '222',
            'value3' => 0,
        ];

        $rules = [
            'value1' => 'contains,aaa bbb ccc',
            'value2' => 'contains,111 222 333',
            'value3' => 'contains,0',
        ];

        $result = (new Validator())->validate($parameters, $rules);

        $this->assertTrue($result);
    }

    public function testInvalid()
    {
        $parameters = [
            'value1' => 'd',
            'value2' => '4',
            'value3' => 1,
        ];

        $rules = [
            'value1' => 'contains,a b c',
            'value2' => 'contains,1 2 3',
            'value3' => 'contains,0',
        ];

        $result = (new Validator())->validate($parameters, $rules);

        $this->assertNotTrue($result);
        $this->assertArrayHasKey('value1', $result);
        $this->assertArrayHasKey('value2', $result);
        $this->assertArrayHasKey('value3', $result);
    }
}
