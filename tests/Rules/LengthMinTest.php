<?php

declare(strict_types=1);

namespace CruxDataValidator\Tests\Rules;

use CruxDataValidator\Validator;
use PHPUnit\Framework\TestCase;

final class LengthMinTest extends TestCase
{
    public function testValid()
    {
        $parameters = [
            'value1' => 'aaaa',
            'value2' => '1234',
            'value3' => 1234,
        ];

        $rules = [
            'value1' => 'min_length,3',
            'value2' => 'min_length,3',
            'value3' => 'min_length,3',
        ];

        $result = (new Validator())->validate($parameters, $rules);

        $this->assertTrue($result);
    }

    public function testInvalid()
    {
        $parameters = [
            'value1' => 0,
            'value2' => null,
            'value3' => 'a',
        ];

        $rules = [
            'value1' => 'min_length,3',
            'value2' => 'min_length,3',
            'value3' => 'min_length,3',
        ];

        $result = (new Validator())->validate($parameters, $rules);

        $this->assertNotTrue($result);
        $this->assertArrayHasKey('value1', $result);
        $this->assertArrayHasKey('value2', $result);
        $this->assertArrayHasKey('value3', $result);
    }
}
