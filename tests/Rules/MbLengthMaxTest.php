<?php

declare(strict_types=1);

namespace CruxDataValidator\Tests\Rules;

use CruxDataValidator\Validator;
use PHPUnit\Framework\TestCase;

final class MbLengthMaxTest extends TestCase
{
    public function testValid()
    {
        $parameters = [
            'value1' => 'йцу',
            'value2' => 123,
            'value3' => 'qwe',
        ];

        $rules = [
            'value1' => 'mb_max_length,3',
            'value2' => 'mb_max_length,3',
            'value3' => 'mb_max_length,3',
        ];

        $result = (new Validator())->validate($parameters, $rules);

        $this->assertTrue($result);
    }

    public function testInvalid()
    {
        $parameters = [
            'value1' => 'йцук',
            'value2' => 'qwer',
            'value3' => 1234,
        ];

        $rules = [
            'value1' => 'mb_max_length,3',
            'value2' => 'mb_max_length,3',
            'value3' => 'mb_max_length,3',
        ];

        $result = (new Validator())->validate($parameters, $rules);

        $this->assertNotTrue($result);
        $this->assertArrayHasKey('value1', $result);
        $this->assertArrayHasKey('value2', $result);
        $this->assertArrayHasKey('value3', $result);
    }
}
