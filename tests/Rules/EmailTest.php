<?php

declare(strict_types=1);

namespace CruxDataValidator\Tests\Rules;

use CruxDataValidator\Validator;
use PHPUnit\Framework\TestCase;

final class EmailTest extends TestCase
{
    public function testValid()
    {
        $parameters = [
            'value1' => 'user.name+tag+sorting@example.com',
            'value2' => '"very.unusual.@.unusual.com"@example.org',
            'value3' => 'disposable.style.email.with+symbol@example.com',
        ];

        $rules = [
            'value1' => 'email',
            'value2' => 'email',
            'value3' => 'email',
        ];

        $result = (new Validator())->validate($parameters, $rules);

        $this->assertTrue($result);
    }

    public function testInvalid()
    {
        $parameters = [
            'value1' => 'A@b@c@example.com',
            'value2' => 'a"b(c)d,e:f;g<h>i[j\k]l@example.com',
            'value3' => 'this\ still\"not\\allowed@example.com',
        ];

        $rules = [
            'value1' => 'email',
            'value2' => 'email',
            'value3' => 'email',
        ];

        $result = (new Validator())->validate($parameters, $rules);

        $this->assertNotTrue($result);
        $this->assertArrayHasKey('value1', $result);
        $this->assertArrayHasKey('value2', $result);
        $this->assertArrayHasKey('value3', $result);
    }
}
