<?php

declare(strict_types=1);

namespace CruxDataValidator\Tests\Rules;

use CruxDataValidator\Validator;
use PHPUnit\Framework\TestCase;

final class CyrillicAlphaTest extends TestCase
{
    public function testValid()
    {
        $parameters = [
            'value1' => 'привет',
            'value2' => 'привет!',
            'value3' => '!привет!',
        ];

        $rules = [
            'value1' => 'cyrillic_alpha',
            'value2' => 'cyrillic_alpha',
            'value3' => 'cyrillic_alpha',
        ];

        $result = (new Validator())->validate($parameters, $rules);

        $this->assertTrue($result);
    }

    public function testInvalid()
    {
        $parameters = [
            'value1' => 'hello',
            'value2' => '12345',
            'value3' => '!@#$%',
        ];

        $rules = [
            'value1' => 'cyrillic_alpha',
            'value2' => 'cyrillic_alpha',
            'value3' => 'cyrillic_alpha',
        ];

        $result = (new Validator())->validate($parameters, $rules);

        $this->assertNotTrue($result);
        $this->assertArrayHasKey('value1', $result);
        $this->assertArrayHasKey('value2', $result);
        $this->assertArrayHasKey('value3', $result);
    }
}
