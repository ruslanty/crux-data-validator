<?php

declare(strict_types=1);

namespace CruxDataValidator\Tests\Rules;

use CruxDataValidator\Validator;
use PHPUnit\Framework\TestCase;

final class DateAfterTest extends TestCase
{
    public function testValid()
    {
        $now = date('Y-m-d');

        $parameters = [
            'value1' => date('Y-m-d', strtotime('tomorrow')),
        ];

        $rules = [
            'value1' => 'date_after,' . $now,
        ];

        $result = (new Validator())->validate($parameters, $rules);

        $this->assertTrue($result);
    }

    public function testInvalid()
    {
        $now = date('Y-m-d');

        $parameters = [
            'value1' => date('Y-m-d', strtotime('yesterday')),
            'value2' => $now,
            'value3' => '',
        ];

        $rules = [
            'value1' => 'date_after,' . $now,
            'value2' => 'date_after,' . $now,
            'value3' => 'date_after,' . $now,
        ];

        $result = (new Validator())->validate($parameters, $rules);

        $this->assertNotTrue($result);
        $this->assertArrayHasKey('value1', $result);
        $this->assertArrayHasKey('value2', $result);
        $this->assertArrayHasKey('value3', $result);
    }
}
