<?php

declare(strict_types=1);

namespace CruxDataValidator\Tests\Rules;

use CruxDataValidator\Validator;
use PHPUnit\Framework\TestCase;

final class SingleSymbolSequenceTest extends TestCase
{
    public function testValid()
    {
        $parameters = [
            'value1' => 'abcde',
            'value2' => 12345,
            'value3' => '12345',
        ];

        $rules = [
            'value1' => 'single_symbol_sequence',
            'value2' => 'single_symbol_sequence,1',
            'value3' => 'single_symbol_sequence,2',
        ];

        $result = (new Validator())->validate($parameters, $rules);

        $this->assertTrue($result);
    }

    public function testInvalid()
    {
        $parameters = [
            'value1' => '1',
            'value2' => 'aa',
            'value3' => 222,
        ];

        $rules = [
            'value1' => 'single_symbol_sequence',
            'value2' => 'single_symbol_sequence,1',
            'value3' => 'single_symbol_sequence,2',
        ];

        $result = (new Validator())->validate($parameters, $rules);

        $this->assertNotTrue($result);
        $this->assertArrayHasKey('value1', $result);
        $this->assertArrayHasKey('value2', $result);
        $this->assertArrayHasKey('value3', $result);
    }
}
