<?php

declare(strict_types=1);

namespace CruxDataValidator\Tests\Rules;

use CruxDataValidator\Validator;
use PHPUnit\Framework\TestCase;

final class NotOnlySpacesTest extends TestCase
{
    public function testValid()
    {
        $parameters = [
            'value1' => 'text ',
            'value2' => ' text ',
            'value3' => '  te xt  ',
        ];

        $rules = [
            'value1' => 'not_only_spaces',
            'value2' => 'not_only_spaces',
            'value3' => 'not_only_spaces',
        ];

        $result = (new Validator())->validate($parameters, $rules);

        $this->assertTrue($result);
    }

    public function testInvalid()
    {
        $parameters = [
            'value1' => ' ',
            'value2' => '    ',
            'value3' => null,
        ];

        $rules = [
            'value1' => 'not_only_spaces',
            'value2' => 'not_only_spaces',
            'value3' => 'not_only_spaces',
        ];

        $result = (new Validator())->validate($parameters, $rules);

        $this->assertNotTrue($result);
        $this->assertArrayHasKey('value1', $result);
        $this->assertArrayHasKey('value2', $result);
        $this->assertArrayHasKey('value3', $result);
    }
}
