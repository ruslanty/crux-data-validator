<?php

declare(strict_types=1);

namespace CruxDataValidator\Tests\Rules;

use CruxDataValidator\Validator;
use PHPUnit\Framework\TestCase;

final class RequiredIfTest extends TestCase
{
    public function testValid()
    {
        $parameters = [
            'value1' => '2',
            'value2' => 1,
            'value3' => 'asd',
        ];

        $rules = [
            'value1' => 'required_if,value2:1',
            'value2' => 'required_if,value1:2',
            'value3' => 'required_if,value1:1',
        ];

        $result = (new Validator())->validate($parameters, $rules);

        $this->assertTrue($result);
    }

    public function testInvalid()
    {
        $parameters = [
            'value1' => '1',
            'value2' => '',
            'value3' => null,
        ];

        $rules = [
            'value11' => 'required_if,value3:null',
            'value22' => 'required_if,value1:1',
            'value33' => 'required_if,value2:',
        ];

        $result = (new Validator())->validate($parameters, $rules);

        $this->assertNotTrue($result);
        $this->assertArrayHasKey('value22', $result);
        $this->assertArrayHasKey('value33', $result);
    }
}
