<?php

declare(strict_types=1);

namespace CruxDataValidator\Tests\Rules;

use CruxDataValidator\Validator;
use PHPUnit\Framework\TestCase;

final class IpTest extends TestCase
{
    public function testValid()
    {
        $parameters = [
            'value1' => '2001:0db8:0000:0000:0000:ff00:0042:8329',
            'value2' => '::ffff:192.0.2.128',
            'value3' => '1.1.1.1',
        ];

        $rules = [
            'value1' => 'ip',
            'value2' => 'ip',
            'value3' => 'ip',
        ];

        $result = (new Validator())->validate($parameters, $rules);

        $this->assertTrue($result);
    }

    public function testInvalid()
    {
        $parameters = [
            'value1' => ip2long('10.128.24.47'),
            'value2' => '.255.255.254',
            'value3' => null,
        ];

        $rules = [
            'value1' => 'ip',
            'value2' => 'ip',
            'value3' => 'ip',
        ];

        $result = (new Validator())->validate($parameters, $rules);

        $this->assertNotTrue($result);
        $this->assertArrayHasKey('value1', $result);
        $this->assertArrayHasKey('value2', $result);
        $this->assertArrayHasKey('value3', $result);
    }
}
