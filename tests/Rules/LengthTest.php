<?php

declare(strict_types=1);

namespace CruxDataValidator\Tests\Rules;

use CruxDataValidator\Validator;
use PHPUnit\Framework\TestCase;

final class LengthTest extends TestCase
{
    public function testValid()
    {
        $parameters = [
            'value1' => 'abc',
            'value2' => '123',
            'value3' => 123,
        ];

        $rules = [
            'value1' => 'length,3',
            'value2' => 'length,3',
            'value3' => 'length,3',
        ];

        $result = (new Validator())->validate($parameters, $rules);

        $this->assertTrue($result);
    }

    public function testInvalid()
    {
        $parameters = [
            'value1' => 'a',
            'value2' => 'aaaa',
            'value3' => null,
        ];

        $rules = [
            'value1' => 'length,3',
            'value2' => 'length,3',
            'value3' => 'length,3',
        ];

        $result = (new Validator())->validate($parameters, $rules);

        $this->assertNotTrue($result);
        $this->assertArrayHasKey('value1', $result);
        $this->assertArrayHasKey('value2', $result);
        $this->assertArrayHasKey('value3', $result);
    }
}
