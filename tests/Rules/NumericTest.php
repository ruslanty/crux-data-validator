<?php

declare(strict_types=1);

namespace CruxDataValidator\Tests\Rules;

use CruxDataValidator\Validator;
use PHPUnit\Framework\TestCase;

final class NumericTest extends TestCase
{
    public function testValid()
    {
        $parameters = [
            'value1' => '123',
            'value2' => '123.123',
            'value3' => 0,
        ];

        $rules = [
            'value1' => 'numeric',
            'value2' => 'numeric',
            'value3' => 'numeric',
        ];

        $result = (new Validator())->validate($parameters, $rules);

        $this->assertTrue($result);
    }

    public function testInvalid()
    {
        $parameters = [
            'value1' => '123,123',
            'value2' => null,
            'value3' => '',
        ];

        $rules = [
            'value1' => 'numeric',
            'value2' => 'numeric',
            'value3' => 'numeric',
        ];

        $result = (new Validator())->validate($parameters, $rules);

        $this->assertNotTrue($result);
        $this->assertArrayHasKey('value1', $result);
        $this->assertArrayHasKey('value2', $result);
        $this->assertArrayHasKey('value3', $result);
    }
}
