<?php

declare(strict_types=1);

namespace CruxDataValidator\Tests\Rules;

use CruxDataValidator\Validator;
use PHPUnit\Framework\TestCase;

final class DateTest extends TestCase
{
    public function testValid()
    {
        $parameters = [
            'value1' => date('Y-m-d'),
        ];

        $rules = [
            'value1' => 'date',
        ];

        $result = (new Validator())->validate($parameters, $rules);

        $this->assertTrue($result);
    }

    public function testInvalid()
    {
        $parameters = [
            'value1' => date('d.m.Y'),
            'value2' => 'now()',
            'value3' => '',
        ];

        $rules = [
            'value1' => 'date',
            'value2' => 'date',
            'value3' => 'date',
        ];

        $result = (new Validator())->validate($parameters, $rules);

        $this->assertNotTrue($result);
        $this->assertArrayHasKey('value1', $result);
        $this->assertArrayHasKey('value2', $result);
        $this->assertArrayHasKey('value3', $result);
    }
}
