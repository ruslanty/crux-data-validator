<?php

declare(strict_types=1);

namespace CruxDataValidator\Tests\Rules;

use CruxDataValidator\Validator;
use PHPUnit\Framework\TestCase;

final class FloatingTest extends TestCase
{
    public function testValid()
    {
        $parameters = [
            'value1' => '1.1',
            'value2' => 2.2,
            'value3' => 8,
        ];

        $rules = [
            'value1' => 'float',
            'value2' => 'float',
            'value3' => 'float',
        ];

        $result = (new Validator())->validate($parameters, $rules);

        $this->assertTrue($result);
    }

    public function testInvalid()
    {
        $parameters = [
            'value1' => 'a',
            'value2' => '0,5',
            'value3' => null,
        ];

        $rules = [
            'value1' => 'float',
            'value2' => 'float',
            'value3' => 'float',
        ];

        $result = (new Validator())->validate($parameters, $rules);

        $this->assertNotTrue($result);
        $this->assertArrayHasKey('value1', $result);
        $this->assertArrayHasKey('value2', $result);
        $this->assertArrayHasKey('value3', $result);
    }
}
