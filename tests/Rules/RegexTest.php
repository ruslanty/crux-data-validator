<?php

declare(strict_types=1);

namespace CruxDataValidator\Tests\Rules;

use CruxDataValidator\Validator;
use PHPUnit\Framework\TestCase;

final class RegexTest extends TestCase
{
    public function testValid()
    {
        $parameters = [
            'value1' => '123',
            'value2' => 123,
            'value3' => 0,
        ];

        $rules = [
            'value1' => 'regex,~\d+~',
            'value2' => 'regex,~\d+~',
            'value3' => 'regex,~\d+~',
        ];

        $result = (new Validator())->validate($parameters, $rules);

        $this->assertTrue($result);
    }

    public function testInvalid()
    {
        $parameters = [
            'value1' => 'asd',
            'value2' => '',
            'value3' => null,
        ];

        $rules = [
            'value1' => 'regex,~\d+~',
            'value2' => 'regex,~\d+~',
            'value3' => 'regex,~\d+~',
        ];

        $result = (new Validator())->validate($parameters, $rules);

        $this->assertNotTrue($result);
        $this->assertArrayHasKey('value1', $result);
        $this->assertArrayHasKey('value2', $result);
        $this->assertArrayHasKey('value3', $result);
    }
}
