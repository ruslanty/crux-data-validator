<?php

declare(strict_types=1);

namespace CruxDataValidator\Tests\Rules;

use CruxDataValidator\Validator;
use PHPUnit\Framework\TestCase;

final class DateBeforeTest extends TestCase
{
    public function testValid()
    {
        $now = date('Y-m-d');

        $parameters = [
            'value1' => date('Y-m-d', strtotime('yesterday')),
        ];

        $rules = [
            'value1' => 'date_before,' . $now,
        ];

        $result = (new Validator())->validate($parameters, $rules);

        $this->assertTrue($result);
    }

    public function testInvalid()
    {
        $now = date('Y-m-d');

        $parameters = [
            'value1' => date('Y-m-d', strtotime('tomorrow')),
            'value2' => $now,
            'value3' => '',
        ];

        $rules = [
            'value1' => 'date_before,' . $now,
            'value2' => 'date_before,' . $now,
            'value3' => 'date_before,' . $now,
        ];

        $result = (new Validator())->validate($parameters, $rules);

        $this->assertNotTrue($result);
        $this->assertArrayHasKey('value1', $result);
        $this->assertArrayHasKey('value2', $result);
        $this->assertArrayHasKey('value3', $result);
    }
}
