<?php

declare(strict_types=1);

namespace CruxDataValidator\Tests\Rules;

use CruxDataValidator\Validator;
use PHPUnit\Framework\TestCase;

final class FieldEqualTest extends TestCase
{
    public function testValid()
    {
        $parameters = [
            'value1' => 'text',
            'value2' => 'text',
            'value3' => 'text',
        ];

        $rules = [
            'value1' => 'equals_field,value1',
            'value2' => 'equals_field,value3',
            'value3' => 'equals_field,value2',
        ];

        $result = (new Validator())->validate($parameters, $rules);

        $this->assertTrue($result);
    }

    public function testInvalid()
    {
        $parameters = [
            'value1' => '',
            'value2' => 0,
            'value3' => null,
        ];

        $rules = [
            'value1' => 'equals_field,value3',
            'value2' => 'equals_field,value1',
            'value3' => 'equals_field,value2',
        ];

        $result = (new Validator())->validate($parameters, $rules);

        $this->assertNotTrue($result);
        $this->assertArrayHasKey('value1', $result);
        $this->assertArrayHasKey('value2', $result);
        $this->assertArrayHasKey('value3', $result);
    }
}
