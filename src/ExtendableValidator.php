<?php

declare(strict_types=1);

namespace CruxDataValidator;

use CruxDataValidator\Rules\RuleInterface;
use InvalidArgumentException;

class ExtendableValidator extends Validator
{
    public function __construct(array $additionalRules = [])
    {
        foreach ($additionalRules as $rule => $class) {
            $ruleClass = new $class;

            if (!$ruleClass instanceof RuleInterface) {
                throw new InvalidArgumentException(
                    sprintf('Rule must implement %s.', RuleInterface::class)
                );
            }
        }

        $this->appendToRuleMap($additionalRules);
    }
}
