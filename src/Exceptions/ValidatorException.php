<?php

declare(strict_types=1);

namespace CruxDataValidator\Exceptions;

use Exception;

final class ValidatorException extends Exception
{
    private $errors;

    public function __construct(int $code = 400, string $message = 'Validation failed.', array $errors = [])
    {
        $this->errors = $errors;

        parent::__construct($message, $code);
    }

    public function getErrors(): array
    {
        return $this->errors;
    }
}
