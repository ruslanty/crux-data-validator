<?php

declare(strict_types=1);

namespace CruxDataValidator;

interface RuleSetInterface
{
    public function getRuleSet(): array;
}
