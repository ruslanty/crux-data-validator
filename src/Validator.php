<?php

declare(strict_types=1);

namespace CruxDataValidator;

use CruxDataValidator\Exceptions\ValidatorException;

class Validator extends BaseValidator
{
    /**
     * @param array $parameters
     * @param array $ruleSet
     * @return array|true
     * @throws \InvalidArgumentException
     */
    public function validate(array $parameters, array $ruleSet)
    {
        return $this->process($parameters, $ruleSet) ?: $this->getErrorMessages();
    }

    /**
     * @param array $parameters
     * @param array $ruleSet
     * @return bool
     * @throws \CruxDataValidator\Exceptions\ValidatorException
     * @throws \InvalidArgumentException
     */
    public function assert(array $parameters, array $ruleSet): bool
    {
        if (!$result = $this->process($parameters, $ruleSet)) {
            throw new ValidatorException(400, 'Validation failed.', $this->getErrorMessages());
        }

        return $result;
    }

    /**
     * @param array $parameters
     * @param array $ruleSets
     * @param array $requiredFields
     * @return array|true
     * @throws \InvalidArgumentException
     */
    public function customValidate(array $parameters, array $ruleSets, array $requiredFields = [])
    {
        $compiledRuleSets = $this->compileCustomRuleSet($ruleSets);

        return $this->process($parameters, $compiledRuleSets, $requiredFields) ?: $this->getErrorMessages();
    }

    /**
     * @param array $parameters
     * @param array $ruleSets
     * @param array $requiredFields
     * @return bool
     * @throws \CruxDataValidator\Exceptions\ValidatorException
     * @throws \InvalidArgumentException
     */
    public function customAssert(array $parameters, array $ruleSets, array $requiredFields = []): bool
    {
        $compiledRuleSets = $this->compileCustomRuleSet($ruleSets);

        if (!$result = $this->process($parameters, $compiledRuleSets, $requiredFields)) {
            throw new ValidatorException(400, 'Validation failed.', $this->getErrorMessages());
        }

        return $result;
    }
}
