<?php

declare(strict_types=1);

namespace CruxDataValidator;

use CruxDataValidator\Rules\Required;
use CruxDataValidator\Rules\RuleInterface;
use CruxDataValidator\Traits\ValidationErrorsTrait;
use InvalidArgumentException;

abstract class BaseValidator extends RuleMap
{
    use ValidationErrorsTrait;

    protected function process(array $parameters, array $ruleSet, array $requiredFields = []): bool
    {
        $this->resetErrorMessages();
        $ruleMap = $this->getRuleMap();

        if ($requiredFields) {
            $this->checkRequiredFields($parameters, $requiredFields);
        }

        foreach ($ruleSet as $field => $rules) {
            if (is_string($rules)) {
                $rules = explode('|', $rules);
            }

            foreach ($rules as $rule) {
                $argument = null; // reset argument

                if (strpos($rule, ',') !== false) {
                    list($rule, $argument) = explode(',', $rule);
                }

                if (empty($ruleMap[$rule]) || !class_exists($ruleMap[$rule])) {
                    throw new InvalidArgumentException(
                        sprintf('Invalid rule %s specified for field %s.', $rule, $field)
                    );
                }

                $ruleClass = new $ruleMap[$rule];
                $this->checkIfValid($ruleClass, $field, $parameters, $argument);
            }
        }

        return empty($this->getErrorMessages());
    }

    protected function compileCustomRuleSet(array $ruleSets): array
    {
        $compiledRuleSet = [];

        foreach ($ruleSets as $ruleSet) {
            if (!$ruleSet instanceof RuleSetInterface) {
                throw new InvalidArgumentException(
                    sprintf('Ruleset must implement %s.', RuleSetInterface::class)
                );
            }

            foreach ($ruleSet->getRuleSet() as $field => $rules) {
                $compiledRuleSet[$field] = $rules;
            }
        }

        return $compiledRuleSet;
    }

    private function checkIfValid(RuleInterface $ruleClass, string $field, array $parameters, string $argument = null)
    {
        if (!$ruleClass->isValid($field, $parameters, $argument)) {
            $this->appendToErrorMessages($field, [
                'field' => $field,
                'argument' => $argument,
                'rule' => $ruleClass->getRuleClass(),
                'error' => str_replace(['{FIELD}', '{ARGUMENT}'], [$field, $argument], $ruleClass->getErrorMessage()),
            ]);
        }
    }

    private function checkRequiredFields(array $parameters, array $requiredFields)
    {
        $requiredRule = new Required();

        foreach ($requiredFields as $field) {
            $this->checkIfValid($requiredRule, $field, $parameters);
        }
    }

    abstract public function validate(array $parameters, array $ruleSet);

    abstract public function assert(array $parameters, array $ruleSet): bool;

    abstract public function customValidate(array $parameters, array $ruleSets, array $requiredFields = []);

    abstract public function customAssert(array $parameters, array $ruleSets, array $requiredFields = []): bool;
}
