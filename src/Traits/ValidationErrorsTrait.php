<?php

declare(strict_types=1);

namespace CruxDataValidator\Traits;

trait ValidationErrorsTrait
{
    private $errorMessages = [];

    protected function getErrorMessages(): array
    {
        return $this->errorMessages;
    }

    private function appendToErrorMessages(string $field, array $errorData)
    {
        $this->errorMessages[$field] = $errorData;
    }

    private function resetErrorMessages()
    {
        $this->errorMessages = [];
    }
}
