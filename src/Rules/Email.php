<?php

declare(strict_types=1);

namespace CruxDataValidator\Rules;

final class Email implements RuleInterface
{
    private $errorMessage = '"{FIELD}" must be valid E-Mail.';

    public function getErrorMessage(): string
    {
        return $this->errorMessage;
    }

    public function getRuleClass(): string
    {
        return __CLASS__;
    }

    public function isValid(string $field, array $parameters, string $argument = null): bool
    {
        if (!array_key_exists($field, $parameters)) {
            return true;
        }

        $parameter = (string)$parameters[$field];

        return filter_var($parameter, FILTER_VALIDATE_EMAIL) !== false;
    }
}
