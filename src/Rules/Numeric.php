<?php

declare(strict_types=1);

namespace CruxDataValidator\Rules;

final class Numeric implements RuleInterface
{
    private $errorMessage = 'The "{FIELD}" field may only contain numeric characters.';

    public function getErrorMessage(): string
    {
        return $this->errorMessage;
    }

    public function getRuleClass(): string
    {
        return __CLASS__;
    }

    public function isValid(string $field, array $parameters, string $argument = null): bool
    {
        if (!array_key_exists($field, $parameters)) {
            return true;
        }

        $parameter = (string)$parameters[$field];

        return is_numeric($parameter);
    }
}
