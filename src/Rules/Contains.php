<?php

declare(strict_types=1);

namespace CruxDataValidator\Rules;

final class Contains implements RuleInterface
{
    private $errorMessage = 'The "{FIELD}" field needs to contain one of these values: "{ARGUMENT}".';

    public function getErrorMessage(): string
    {
        return $this->errorMessage;
    }

    public function getRuleClass(): string
    {
        return __CLASS__;
    }

    public function isValid(string $field, array $parameters, string $argument = null): bool
    {
        if (!array_key_exists($field, $parameters)) {
            return true;
        }

        $parameter = (string)$parameters[$field];
        $argument = strtolower(trim($argument));
        $value = strtolower(trim($parameter));

        if (preg_match_all('~\'(.+?)\'~', $argument, $matches, PREG_PATTERN_ORDER)) {
            $arguments = $matches[1];
        } else {
            $arguments = explode(' ', $argument);
        }

        return in_array($value, $arguments, true);
    }
}
