<?php

declare(strict_types=1);

namespace CruxDataValidator\Rules;

final class Length implements RuleInterface
{
    private $errorMessage = 'The "{FIELD}" field needs to be exactly {ARGUMENT} characters in length.';

    public function getErrorMessage(): string
    {
        return $this->errorMessage;
    }

    public function getRuleClass(): string
    {
        return __CLASS__;
    }

    public function isValid(string $field, array $parameters, string $argument = null): bool
    {
        if (!array_key_exists($field, $parameters)) {
            return true;
        }

        $parameter = (string)$parameters[$field];

        return strlen($parameter) === (int)$argument;
    }
}
