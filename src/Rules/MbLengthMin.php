<?php

declare(strict_types=1);

namespace CruxDataValidator\Rules;

final class MbLengthMin implements RuleInterface
{
    private $errorMessage = 'The "{FIELD}" field needs to be {ARGUMENT} or longer in length.';

    public function getErrorMessage(): string
    {
        return $this->errorMessage;
    }

    public function getRuleClass(): string
    {
        return __CLASS__;
    }

    public function isValid(string $field, array $parameters, string $argument = null): bool
    {
        if (!array_key_exists($field, $parameters)) {
            return true;
        }

        $parameter = (string)$parameters[$field];

        return mb_strlen($parameter, 'UTF-8') >= (int)$argument;
    }
}
