<?php

declare(strict_types=1);

namespace CruxDataValidator\Rules;

final class Required implements RuleInterface
{
    private $errorMessage = 'The "{FIELD}" field is required.';

    public function getErrorMessage(): string
    {
        return $this->errorMessage;
    }

    public function getRuleClass(): string
    {
        return __CLASS__;
    }

    public function isValid(string $field, array $parameters, string $argument = null): bool
    {
        return isset($parameters[$field])
            && ($parameters[$field] === false
                || $parameters[$field] === 0
                || $parameters[$field] === 0.0
                || $parameters[$field] === '0'
                || !empty($parameters[$field])
            );
    }
}
