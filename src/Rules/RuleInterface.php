<?php

declare(strict_types=1);

namespace CruxDataValidator\Rules;

interface RuleInterface
{
    public function getErrorMessage(): string;

    public function getRuleClass(): string;

    public function isValid(string $field, array $parameters, string $argument = null): bool;
}
