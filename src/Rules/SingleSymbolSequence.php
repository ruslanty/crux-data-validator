<?php

declare(strict_types=1);

namespace CruxDataValidator\Rules;

final class SingleSymbolSequence implements RuleInterface
{
    private $errorMessage = 'Single symbol sequence is not allowed for the "{FIELD}" field.';

    public function getErrorMessage(): string
    {
        return $this->errorMessage;
    }

    public function getRuleClass(): string
    {
        return __CLASS__;
    }

    public function isValid(string $field, array $parameters, string $argument = null): bool
    {
        if (!array_key_exists($field, $parameters)) {
            return true;
        }

        if ($argument > $parameters[$field]) {
            return true;
        }

        $parameter = (string)$parameters[$field];
        $length = mb_strlen($parameter);
        $sequencePart = mb_strcut($parameter, (int)$argument, $length);
        $testedLength = mb_strlen($sequencePart);

        for ($i = 0; $i < $testedLength; $i++) {
            $currentChar = ord(strtolower($sequencePart[$i]));

            if (isset($sequencePart[$i + 1])) {
                $nextChar = ord(strtolower($sequencePart[$i + 1]));

                if ($currentChar !== $nextChar) {
                    //successful passing the test exit point
                    return true;
                }
            }
        }

        return false;
    }
}
