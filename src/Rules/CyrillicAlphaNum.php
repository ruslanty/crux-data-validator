<?php

declare(strict_types=1);

namespace CruxDataValidator\Rules;

final class CyrillicAlphaNum implements RuleInterface
{
    private $errorMessage = '"{FIELD}" must contain cyrillic letters + numbers.';

    public function getErrorMessage(): string
    {
        return $this->errorMessage;
    }

    public function getRuleClass(): string
    {
        return __CLASS__;
    }

    public function isValid(string $field, array $parameters, string $argument = null): bool
    {
        if (!array_key_exists($field, $parameters)) {
            return true;
        }

        $parameter = (string)$parameters[$field];
        $pattern = '~^(?=.*[\p{Cyrillic}])[\p{Cyrillic}[:punct:]\s\d]+$~u';

        return (bool)preg_match($pattern, $parameter);
    }
}
