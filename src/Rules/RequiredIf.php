<?php

declare(strict_types=1);

namespace CruxDataValidator\Rules;

final class RequiredIf implements RuleInterface
{
    private $errorMessage = 'The "{FIELD}" field is required because of "{ARGUMENT}".';

    public function getErrorMessage(): string
    {
        return $this->errorMessage;
    }

    public function getRuleClass(): string
    {
        return __CLASS__;
    }

    public function isValid(string $field, array $parameters, string $argument = null): bool
    {
        if (strpos($argument, ':') === false) {
            return false;
        }

        list($dataField, $dataValues) = explode(':', $argument);
        $values = explode(' ', $dataValues);

        return !array_key_exists($dataField, $parameters)
            || !in_array($parameters[$dataField], $values, true)
            || (isset($parameters[$field])
                && ($parameters[$field] === false
                    || $parameters[$field] === 0
                    || $parameters[$field] === 0.0
                    || $parameters[$field] === '0'
                    || !empty($parameters[$field])
                )
            );
    }
}
