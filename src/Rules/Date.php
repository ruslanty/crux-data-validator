<?php

declare(strict_types=1);

namespace CruxDataValidator\Rules;

final class Date implements RuleInterface
{
    private $errorMessage = 'The "{FIELD}" field needs to be a valid date, of "Y-m-d" format.';

    public function getErrorMessage(): string
    {
        return $this->errorMessage;
    }

    public function getRuleClass(): string
    {
        return __CLASS__;
    }

    public function isValid(string $field, array $parameters, string $argument = null): bool
    {
        if (!array_key_exists($field, $parameters)) {
            return true;
        }

        $parameter = (string)$parameters[$field];

        if (!preg_match('~^\d{4}-\d{2}-\d{2}$~', $parameter)) {
            return false;
        }

        list($year, $month, $day) = array_map('intval', explode('-', $parameter));

        return checkdate($month, $day, $year);
    }
}
