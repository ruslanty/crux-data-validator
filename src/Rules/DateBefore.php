<?php

declare(strict_types=1);

namespace CruxDataValidator\Rules;

use DateTime;
use Exception;

final class DateBefore implements RuleInterface
{
    private $errorMessage = 'The "{FIELD}" field needs to be a valid date, of "Y-m-d" format BEFORE {ARGUMENT}.';

    public function getErrorMessage(): string
    {
        return $this->errorMessage;
    }

    public function getRuleClass(): string
    {
        return __CLASS__;
    }

    public function isValid(string $field, array $parameters, string $argument = null): bool
    {
        if (!array_key_exists($field, $parameters)) {
            return true;
        }

        $parameter = (string)$parameters[$field];

        if (!preg_match('~^\d{4}-\d{2}-\d{2}$~', $parameter)) {
            return false;
        }

        try {
            $date = new DateTime($parameter);
            $limitDate = new DateTime($argument);
        } catch (Exception $e) {
            return false;
        }

        return $limitDate->getTimestamp() > $date->getTimestamp();
    }
}
