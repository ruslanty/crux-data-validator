<?php

declare(strict_types=1);

namespace CruxDataValidator;

abstract class RuleMap
{
    private $ruleMap = [
        'required' => \CruxDataValidator\Rules\Required::class,
        'required_if' => \CruxDataValidator\Rules\RequiredIf::class,
        'integer' => \CruxDataValidator\Rules\Integer::class,
        'float' => \CruxDataValidator\Rules\Floating::class,
        'length' => \CruxDataValidator\Rules\Length::class,
        'min_length' => \CruxDataValidator\Rules\LengthMin::class,
        'max_length' => \CruxDataValidator\Rules\LengthMax::class,
        'contains' => \CruxDataValidator\Rules\Contains::class,
        'numeric' => \CruxDataValidator\Rules\Numeric::class,
        'ip' => \CruxDataValidator\Rules\Ip::class,
        'email' => \CruxDataValidator\Rules\Email::class,
        'regex' => \CruxDataValidator\Rules\Regex::class,
        'equals_field' => \CruxDataValidator\Rules\FieldEqual::class,
        'not_only_spaces' => \CruxDataValidator\Rules\NotOnlySpaces::class,
        'cyrillic_alpha' => \CruxDataValidator\Rules\CyrillicAlpha::class,
        'cyrillic_alpha_num' => \CruxDataValidator\Rules\CyrillicAlphaNum::class,
        'mb_length' => \CruxDataValidator\Rules\MbLength::class,
        'mb_min_length' => \CruxDataValidator\Rules\MbLengthMin::class,
        'mb_max_length' => \CruxDataValidator\Rules\MbLengthMax::class,
        'date' => \CruxDataValidator\Rules\Date::class,
        'date_before' => \CruxDataValidator\Rules\DateBefore::class,
        'date_after' => \CruxDataValidator\Rules\DateAfter::class,
        'single_symbol_sequence' => \CruxDataValidator\Rules\SingleSymbolSequence::class,
    ];

    public function getRuleMap(): array
    {
        return $this->ruleMap;
    }

    protected function appendToRuleMap(array $additionalRules)
    {
        $this->ruleMap = array_merge($this->ruleMap, $additionalRules);
    }
}
